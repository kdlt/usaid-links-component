/**
 * USAID Universal Footer Links JS.
 * @file
 */
(function (Drupal, drupalSettings) {
  Drupal.behaviors.usaidMainLogo = {
    attach: function attach(context, settings) {
      // The below will alter the image path for the hover state on the footer main logos.
      // Define variables.
      const main_logo = document.getElementById("uls-main-logo")
      const main_logo_link = document.getElementById("uls-main-logo-link")
      const container = document.getElementById("uls-partner-container")

      // Custom events for the functions below.
      main_logo_link.onmouseover = logMouseOver
      main_logo_link.onmouseout = logMouseOut

      // Main logo image path.
      const src_path = main_logo.dataset.imgpath
      // Mouse over function.
      function logMouseOver() {
        // Dark BG.
        if (container.classList.contains('uls-partner--style-dark')) {
          // Alter the path.
          main_logo.src = src_path + "logo-main--style-dark--hover.svg"
        }
        // Light BG.
        if (container.classList.contains('uls-partner--style-light')) {
          // Alter the path.
          main_logo.src = src_path + "logo-main--style-light--hover.svg"
        }
      }

      // Mouse out function.
      function logMouseOut() {
        // Dark BG.
        if (container.classList.contains('uls-partner--style-dark')) {
          // Alter the path.
          main_logo.src = src_path + "logo-main--style-dark.svg"
        }
        // Light BG.
        if (container.classList.contains('uls-partner--style-light')) {
          // Alter the path.
          main_logo.src = src_path + "logo-main--style-light.svg"
        }
      }
    }
  };
})(Drupal, drupalSettings);
