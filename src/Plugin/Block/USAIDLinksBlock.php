<?php

namespace Drupal\usaid_link_sites\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Symfony\Component\Yaml\Parser;

/**
 * Provides a custom links block.
 *
 * @Block(
 *   id = "usaid_link_sites",
 *   admin_label = @Translation("USAID block for footer links.")
 * )
 */
class USAIDLinksBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Set the array.
    $build = [];
    // Pull in the yaml data and use the Symfony parser.
    // D8 way of getting a module path.
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('usaid_link_sites')->getPath();
    // Define the path to the data file.
    $path_to_file = $module_path . '/data/uls_links.yaml';
    // Instantiate the Symfony YAML parser
    // as defined above in the use statement.
    $parser = new Parser();
    // Parse the data from the file.
    $data = $parser->parse(file_get_contents($path_to_file));
    // Now derive the yaml data array.
    $config_data = $data["links"];

    // Get the module config form setting.
    $config = \Drupal::service('config.factory')->getEditable('usaid_link_sites.adminsettings');
    // Build the block.
    // We set variables for the twig template
    // that match what is defined in hook_theme.
    $build['#theme'] = 'usaid_link_sites';

    // Check for usaid_links_style.
    if (!empty($config->getRawData()['usaid_links_style'])) {
      $build['#config_style'] = $config->getRawData()['usaid_links_style'];
    }

    // Check for config_width.
    if (!empty($config->getRawData()['config_width'])) {
      $build['#config_width'] = $config->getRawData()['usaid_links_width'];
    }

    // Other misc settings and data.
    $build['#config_data'] = $config_data;
    $build['#title'] = '';
    $build['#path'] = $module_path;

    return $build;
  }

}
